# Change Log

## 0.2.1 - 2017-01-30

Updated to support Internet Explorer 8. Switched to an explicit calculation
button due to lack of support for detecting form "input" events in IE8. Added
polyfills to allow use of console logging and React.


## 0.1.0 - 2017-01-12

Initial release of ClojureScript version, converted from Clem Sturmfels' Dam
Bank Volume Calculator.
