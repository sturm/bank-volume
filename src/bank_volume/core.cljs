;; Dam Bank Volume Calculator
;; Copyright (C) 2017 Ben Sturmfels

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(ns bank-volume.core
  (:require
   ;; We don't actually use Om in this app. IE8 support may be an issue if we did.
   ;; I know that React dropped support for IE8 in v15.0.0.
   [om.core :as om :include-macros true]
   [om.dom :as dom :include-macros true]
   goog.dom
   goog.events))


(enable-console-print!)

;; All logging to console disabled as it causes errors in IE8 when the Developer
;; Tools aren't open.

(println "This text is printed from src/bank-volume/core.cljs. Go ahead and edit it and see reloading in action.")

;; define your app data so that it doesn't get over-written on reload

(defonce app-state (atom {:text "Hello, World!"}))

(defn nandash
  [number]
  (if (js/isNaN number)
    "–"
    number))

(defn end-area
  [r upstream-batter downstream-batter crest-width crest-level]
  (let [
        bank-height (js/Math.abs (- r crest-level))
        end-area (+ (* crest-width bank-height) (* (+ upstream-batter downstream-batter) bank-height (/ bank-height 2)))]
    end-area))

(defn recalculate
  []
  (println "Recalculating...")
  (let [
        ;; Read inputs.
        readings (filter #(not (js/isNaN %))
                         (map #(js/parseFloat (.-value (goog.dom/getElement %)))
                              '("r1" "r2" "r3" "r4" "r5" "r6" "r7" "r8" "r9" "r10" "r11" "r12" "r13" "r14" "r15")))
        distances (filter #(not (js/isNaN %))
                          (map #(js/parseFloat (.-value (goog.dom/getElement %)))
                               '("d1" "d2" "d3" "d4" "d5" "d6" "d7" "d8" "d9" "d10" "d11" "d12" "d13" "d14")))
        ub (js/parseFloat (.-value (goog.dom/getElement "ub")))
        db (js/parseFloat (.-value (goog.dom/getElement "db")))
        cw (js/parseFloat (.-value (goog.dom/getElement "cw")))
        cl (js/parseFloat (.-value (goog.dom/getElement "cl")))
        cost-per-m3 (js/parseFloat (.-value (goog.dom/getElement "cost-per-m3")))

        ;; Do the calculations.
        end-areas (map #(end-area % ub db cw cl) readings)
        avg-end-areas (map #(/ (+ %1 %2) 2) end-areas (drop 1 end-areas))
        segment-volumes (map #(* %1 %2) avg-end-areas distances)
        total-volume (apply + segment-volumes)
        total-cost (* total-volume cost-per-m3)]

    (println "Readings: " readings)
    (println "Distances: " distances)
    (println "end-areas: " end-areas)
    (println "avg-end-areas: " avg-end-areas)
    (println "segment-volumes: " segment-volumes)
    (println "Total volume: " total-volume)
    (println "Total cost: " total-cost)

    ;; Update outputs.
    (goog.dom/setTextContent (goog.dom/getElement "volume") (nandash (.toFixed total-volume 0)))
    (goog.dom/setTextContent (goog.dom/getElement "cost") (nandash (.toFixed total-cost 0)))

    ))

(om/root
  (fn [data _owner]
    (reify om/IRender
      (render [_]
        (dom/h1 nil (:text data)))))
  app-state
  {:target (. js/document (getElementById "app"))})

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)

(defn onload
  []
  ;; The goog.events module is important to support IE8, as it
  ;; uses "attachEvent" rather than "addEventListener".
  ;; (goog.events/listen (goog.dom/getElement "volume-form") "input" recalculate)
  ;; Adding an explicit "Calculate" button for IE8 only, as it doesn't support
  ;; the "input" event.
  (goog.events/listen (goog.dom/getElement "calculate") "click" recalculate)
  (recalculate))

(set! js/window.onload onload)
